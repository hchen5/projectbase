using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using static UnityEngine.GraphicsBuffer;

namespace UF2BaseProject
{
    public class EnemySM : MonoBehaviour
    {
        [SerializeField]
        private float _Speed = 3f;
        [SerializeField]
        private int _Attack1DMG = 1;
        public int AttackDMG { get { return _Attack1DMG; } }
        [SerializeField]
        private int _Health;
        private Animator _Animator;
        private Rigidbody2D _Rigidbody;
        private CircleCollider2D _FollowCircleCollider2D;
        private SpriteRenderer _SpriteRenderer; 
        [SerializeField]
        private SimpleGameEvent _GameEventEnemyKilled;
        public enum MachineStates{ None, Idle, Follow, Attack1, FollowY}
        private MachineStates _CurrentState;

        private void Awake()
        {
            _Rigidbody = GetComponent<Rigidbody2D>();
            _Animator = GetComponent<Animator>();
            _FollowCircleCollider2D = transform.GetChild(0).GetComponent<CircleCollider2D>();
            _SpriteRenderer = GetComponent<SpriteRenderer>();
        }
        public void Init(EnemySO ESO, Transform spawnposition) 
        {
            transform.localPosition = spawnposition.position;
            _Speed = ESO._Speed;
            _Attack1DMG = ESO._AttackDMG;
            _FollowCircleCollider2D.radius = ESO._FollowRange;
            _SpriteRenderer.color = ESO._Color;
            _Health = ESO._Health;
        }
        private void Start()
        {
            InitState(MachineStates.Idle);
        }
        private void ChangeState(MachineStates newState)
        {
            if (newState == _CurrentState)
                return;
            ExitState();
            InitState(newState);
        }
        private Transform _Target;
        public void FollowPlayer(Transform Player) 
        {
            _Target = Player;
            ChangeState(MachineStates.Follow);
        }
        public void StopFollowPlayer() 
        {
            _Target = null;
            ChangeState(MachineStates.Idle);
        }
        public void AttackPlayer(Transform Player) 
        {
            _Target = Player;
            ChangeState(MachineStates.Attack1);
        }
        private void Update()
        {
            UpdateState();
        }
        private void InitState(MachineStates currentState)
        {
            _CurrentState = currentState;
            switch (_CurrentState)
            {
                case MachineStates.Idle:
                    _Rigidbody.velocity = Vector2.zero;
                    _Animator.Play("EnemyBaseIdle");
                    break;

                case MachineStates.Follow:
                    _Animator.Play("EnemyBaseWalk");
                    break;
                case MachineStates.FollowY:
                    _Animator.Play("EnemyBaseWalk");
                    break;
                case MachineStates.Attack1:
                    _Rigidbody.velocity = Vector2.zero;
                    //m_HitboxInfo.Damage = m_Hit1Damage;
                    _Animator.Play("EnemyBaseAttack");
                    break;

                default:
                    break;
            }
        }
        private void UpdateState()
        {
            switch (_CurrentState)
            {
                case MachineStates.Idle:

                    break;
                case MachineStates.Follow:
                    if (_Target != null)
                    {
                        Vector2 direction = (_Target.position - transform.position).normalized;
                        //_SpriteRenderer.flipX = direction.x < 0;
                        _Rigidbody.velocity = direction * _Speed;
                        if (direction.x < 0) 
                        {
                            transform.localEulerAngles = Vector3.up * 180;
                        }else if(direction.x >0)
                            transform.localEulerAngles = Vector3.up * 0;
                    }
                    break;
                    case MachineStates.FollowY:
                    float yDifference = _Target.position.y - transform.position.y;
                    if (yDifference > 0.1f || yDifference < -0.1f)
                        _Rigidbody.velocity = (Vector2.up * yDifference).normalized * _Speed;
                    else
                    {
                        ChangeState(MachineStates.Attack1);
                    }
                    break;
                case MachineStates.Attack1:
                    /*
                    float yDifference1 = _Target.position.y - transform.position.y;
                    if (yDifference1 > 0.1f || yDifference1 < -0.1f)
                        _Rigidbody.velocity = (Vector2.up * yDifference1).normalized * _Speed;
                    else
                    {
                        _Rigidbody.velocity = Vector2.zero;
                        if (_AttackCoroutine == null)
                        {
                            _AttackCoroutine = StartCoroutine(Attack());
                        }
                    }*/
                    float yDifference1 = _Target.position.y - transform.position.y;
                    if (yDifference1 > 0.1f || yDifference1 < -0.1f)
                        ChangeState(MachineStates.FollowY);
                    else if (_AttackCoroutine == null)
                        _AttackCoroutine = StartCoroutine(Attack());
                    break;
                default:
                    break;
            }
        }
        private Coroutine _AttackCoroutine;
        IEnumerator Attack() 
        {
            while (true)
            {
                int a = Random.Range(1, 3);
                _Animator.Play("EnemyBaseIdle");
                yield return new WaitForSeconds(a);
                _Animator.Play("EnemyBaseAttack");
            }
        }
        private void ExitState()
        {
            switch (_CurrentState)
            {
                case MachineStates.Idle:

                    break;

                case MachineStates.Follow:

                    break;

                case MachineStates.Attack1:
                    if(_AttackCoroutine !=null)
                        StopCoroutine(_AttackCoroutine);
                    _AttackCoroutine= null;
                    break;
                default:
                    break;
            }
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            //if (collision.gameObject.layer == LayerMask.NameToLayer("PlayerHitBox")) 
            //{ 
            if (collision.gameObject.GetComponent<PlayerHitBox>() != null)
            {
                SubstractHealth(collision.gameObject.GetComponent<PlayerHitBox>().DMG);
            }
            //}
        }
        public void SubstractHealth(int health)
        {
            if (_Health - health > 0)
                _Health -= health;
            else
                _Health = 0;

            if (_Health == 0)
            {
                Destroy(gameObject);
                _GameEventEnemyKilled.Raise();
            }
        }
    }
}
