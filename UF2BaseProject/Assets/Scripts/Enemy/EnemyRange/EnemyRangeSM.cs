using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using static UnityEngine.GraphicsBuffer;

namespace UF2BaseProject
{
    public class EnemyRangeSM : MonoBehaviour
    {
        [SerializeField]
        private float _Speed = 3f;
        [SerializeField]
        private int _Attack1DMG = 2;
        public int AttackDMG { get { return _Attack1DMG; } }
        [SerializeField]
        private int _Health;
        private Animator _Animator;
        private Rigidbody2D _Rigidbody;
        private Transform[] _WayPoints ;
        private int _WaypointIndex = 0;
        private CircleCollider2D _FollowCircleCollider2D;
        private SpriteRenderer _SpriteRenderer;
        [SerializeField]
        private SimpleGameEvent _GameEventEnemyKilled;
        public enum MachineStates { None, Idle, Follow, FollowY, Attack1, Patroling }
        private MachineStates _CurrentState;
        private void Awake()
        {
            _Rigidbody = GetComponent<Rigidbody2D>();
            _Animator = GetComponent<Animator>();
            _FollowCircleCollider2D = transform.GetChild(0).GetComponent<CircleCollider2D>();
            _SpriteRenderer = GetComponent<SpriteRenderer>();
        }
        private void Start()
        {
            InitState(MachineStates.Idle);
        }
        public void Init(Transform[] transforms,Transform _SpawnPoint,EnemySO ESO) 
        {
            Assert.IsNotNull(transforms);
            Assert.IsNotNull(_SpawnPoint); 
            transform.position = _SpawnPoint.position;
            _WayPoints = transforms;

            _Speed = ESO._Speed;
            _Attack1DMG = ESO._AttackDMG;
            _FollowCircleCollider2D.radius = ESO._FollowRange;
            _SpriteRenderer.color = ESO._Color;
            _Health = ESO._Health;

        }
        private void Patroling()
        {
            _Animator.Play("EnemyRangeWalk");
            if (_WayPoints.Length > 0)
            {
                if (_WaypointIndex != -1)
                {
                    Vector2 direc = (_WayPoints[_WaypointIndex].transform.position - transform.position).normalized;
                    _Rigidbody.velocity =  direc * _Speed;
                    if (direc.x < 0)
                    {
                        transform.localEulerAngles = Vector3.up * 180;
                    }
                    else if (direc.x > 0)
                        transform.localEulerAngles = Vector3.up * 0;
                    if (Vector2.Distance(transform.position, _WayPoints[_WaypointIndex].position) < Time.fixedDeltaTime * _Speed)//Time.fixedDeltaTime * _Speed
                    {
                        if (_WayPoints.Length - 1 == _WaypointIndex)
                        {
                            _WaypointIndex = 0;
                        }
                        else
                        {
                            _WaypointIndex++;
                        }
                    }
                }
                else
                    _Rigidbody.velocity = Vector3.zero;
            }
        }
        private void ChangeState(MachineStates newState)
        {
            if (newState == _CurrentState)
                return;
            ExitState();
            InitState(newState);
        }
        private void InitState(MachineStates currentState)
        {
            _CurrentState = currentState;
            switch (_CurrentState)
            {
                case MachineStates.Idle:
                    _Rigidbody.velocity = Vector2.zero;
                    _Animator.Play("EnemyRangeIdle");
                    break;

                case MachineStates.Follow:
                    _Animator.Play("EnemyRangeWalk");
                    break;
                case MachineStates.Patroling:
                    _Animator.Play("EnemyRangeWalk");
                    break;
                case MachineStates.Attack1:
                    _Rigidbody.velocity = Vector2.zero;
                    //m_HitboxInfo.Damage = m_Hit1Damage;
                    //_Animator.Play("EnemyRangeAttack");
                    break;
                case MachineStates.FollowY:
                    _Animator.Play("EnemyRangeWalk");
                    break;
                default:
                    break;
            }
        }
        private Transform _Target;
        private Coroutine _AttackCoroutine;
        [SerializeField]
        private GameObject _AttackBall;
        private void UpdateState()
        {
            switch (_CurrentState)
            {
                case MachineStates.Idle:
                    if (_Target == null)
                        StartCoroutine(wait());
                    break;
                case MachineStates.Follow:
                    if (_Target != null)
                    {
                        _Rigidbody.velocity = (_Target.position - transform.position).normalized * _Speed;
                    }
                    Flip();
                    break;
                    
                case MachineStates.FollowY:
                    Flip();
                    float yDifference = _Target.position.y - transform.position.y;
                    if (yDifference > 0.1f || yDifference < -0.1f)
                        _Rigidbody.velocity = (Vector2.up * yDifference).normalized * _Speed;
                    else
                        ChangeState(MachineStates.Attack1);
                    break;
                case MachineStates.Attack1:
                    Flip();
                    //_Rigidbody.velocity = Vector2.zero;
                    /*
                    float yDifference1 = _Target.position.y - transform.position.y;
                    if (yDifference1 > 0.1f || yDifference1 < -0.1f)
                        _Rigidbody.velocity = (Vector2.up * yDifference1).normalized * _Speed;
                    else if (Vector2.Distance(_Target.position, transform.position) > 0.5f)
                    {
                        _Rigidbody.velocity = Vector2.zero;
                        if (_AttackCoroutine == null)
                        {
                            _AttackCoroutine = StartCoroutine(Attack());
                        }
                    }
                    else
                    {
                        _Rigidbody.velocity = Vector2.zero;
                        if (_AttackCoroutine == null)
                        {
                            _AttackCoroutine = StartCoroutine(Attack());
                        }
                    }*/
                    
                    float yDifference1 = _Target.position.y - transform.position.y;
                    if (yDifference1 > 0.1f || yDifference1 < -0.1f)
                        ChangeState(MachineStates.FollowY);
                    else if (_AttackCoroutine == null)
                            _AttackCoroutine = StartCoroutine(Attack());
                    break;
                case MachineStates.Patroling:
                    Patroling();
                    break;
                default:
                    break;
            }
        }
        private void Flip()
        {
            if (_Target != null)
            {
                Vector2 direction = (_Target.position - transform.position).normalized;
                if (direction.x < 0)
                {
                    transform.localEulerAngles = Vector3.up * 180;
                }
                else if (direction.x > 0)
                    transform.localEulerAngles = Vector3.up * 0;
            }
        }
        public void FollowPlayer(Transform Player)
        {
            _Target = Player;
            ChangeState(MachineStates.Follow);
        }
        public void StopFollowPlayer()
        {
            _Target = null;
            ChangeState(MachineStates.Idle);
        }
        private IEnumerator wait() 
        {
            yield return new WaitForSeconds(1);
            ChangeState(MachineStates.Patroling);
        }
        public void AttackPlayer(Transform Player)
        {
            _Target = Player;
            //ChangeState(MachineStates.Attack1);
            ChangeState(MachineStates.FollowY);
        }
        IEnumerator Attack()
        {
            while (true)
            {
                int a = Random.Range(1, 4);
                _Animator.Play("EnemyRangeIdle");
                yield return new WaitForSeconds(a);
                _Animator.Play("EnemyRangeAttack");
                yield return new WaitForSeconds(0.80f);
                GameObject g = Instantiate(_AttackBall);
                g.transform.position = transform.position;
                Vector2 direction = (_Target.position - g.transform.position).normalized;
                g.GetComponent<Rigidbody2D>().velocity =  direction * 10f;
                g.GetComponent<EnemyRangeHitbox>().Damage = _Attack1DMG;
                if (direction.x < 0)
                {
                    g.transform.localEulerAngles = Vector3.up * 180;
                }
                else if (direction.x > 0)
                    g.transform.localEulerAngles = Vector3.up * 0;
                Flip();
            }
        }
        private void ExitState()
        {
            switch (_CurrentState)
            {
                case MachineStates.Idle:

                    break;

                case MachineStates.Follow:

                    break;
                case MachineStates.FollowY:
                    break;
                case MachineStates.Attack1:
                    if(_AttackCoroutine != null)
                        StopCoroutine(_AttackCoroutine);
                    _AttackCoroutine = null;
                    break;
                case MachineStates.Patroling:

                    break;
                default:
                    break;
            }
        }
        private void Update()
        {
            UpdateState();
        }
        [SerializeField]
        private LayerMask PlayerHitBox;
        private void OnTriggerEnter2D(Collider2D collision)
        {
            //if (collision.gameObject.layer == LayerMask.NameToLayer("PlayerHitBox")) 
            //{ 
            if (collision.gameObject.GetComponent<PlayerHitBox>() != null)
            {
                SubstractHealth(collision.gameObject.GetComponent<PlayerHitBox>().DMG);
            }
            //}
        }
        public void SubstractHealth(int health)
        {
            if (_Health - health > 0)
                _Health -= health;
            else
                _Health = 0;

            if (_Health == 0)
            {
                Destroy(gameObject);
                _GameEventEnemyKilled.Raise();
            }
        }
    }
}
