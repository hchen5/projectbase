using System.Collections;
using System.Collections.Generic;
using UF2BaseProject;
using UnityEngine;

public class EnemyRangeAttack : MonoBehaviour
{
    private EnemyRangeSM _EnemyRangeSM;
    private void Awake()
    {
        _EnemyRangeSM = GetComponentInParent<EnemyRangeSM>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            _EnemyRangeSM.AttackPlayer(collision.transform);

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            _EnemyRangeSM.FollowPlayer(collision.transform);
    }
}
