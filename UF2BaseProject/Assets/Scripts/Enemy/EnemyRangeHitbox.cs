using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRangeHitbox : MonoBehaviour
{
    [HideInInspector]
    public int Damage;

    private void Start()
    {
        DestroyOBject();
    }
    private void DestroyOBject() 
    {
        Destroy(gameObject, 5f);
    }
}
