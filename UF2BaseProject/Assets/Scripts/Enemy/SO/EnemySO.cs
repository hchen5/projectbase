using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemySO", menuName = "ScriptableObjects/EnemySO")]
public class EnemySO : ScriptableObject
{
    public enum EnemyType { Melee, Range}
    public EnemyType _EnemyType;
    public float _Speed;
    public Color _Color;
    public float _FollowRange;
    public int _AttackDMG;
    public int _Health;
}
