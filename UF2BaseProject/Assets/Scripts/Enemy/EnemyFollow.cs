using System.Collections;
using System.Collections.Generic;
using UF2BaseProject;
using UnityEngine;

public class EnemyFollow : MonoBehaviour
{
    private EnemySM _EnemySM;
    private void Awake()
    {
        _EnemySM = GetComponentInParent<EnemySM>();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            _EnemySM.FollowPlayer(collision.transform);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
            _EnemySM.StopFollowPlayer();
    }
}
