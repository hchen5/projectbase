using System.Collections;
using System.Collections.Generic;
using TMPro;
using UF2BaseProject;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    [SerializeField]
    private Player _Player;
    [SerializeField] 
    private Spawner _Spawner;
    [SerializeField]
    private TextMeshProUGUI _PlayerHealth;
    [SerializeField]
    private TextMeshProUGUI _Wave;

    private void OnEnable()
    {
        _Player.OnHealthChange += UpdateCharacterHealth;
        _Spawner.OnChangeWave += UpdateWaveText;
    }
    private void UpdateWaveText(int wave) 
    {
        _Wave.SetText("Wave: "+ wave);
    }
    private void UpdateCharacterHealth(int health) 
    {
        _PlayerHealth.SetText("Vida: " + health);
    }
    private void OnDisable()
    {
        _Player.OnHealthChange -= UpdateCharacterHealth;
        _Spawner.OnChangeWave -= UpdateWaveText;
    }
}
