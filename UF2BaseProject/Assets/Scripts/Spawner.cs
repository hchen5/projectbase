using System.Collections;
using System.Collections.Generic;
using UF2BaseProject;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private Transform[] _WaypointsRangeEnemy;
    [SerializeField]
    private Transform[] _SpawnPoint;
    [SerializeField]
    private GameObject _RangedEnemy;
    [SerializeField]
    private GameObject _MeleeEnemy;
    [SerializeField]
    private EnemySO[] _MeleeSO;
    [SerializeField]
    private EnemySO[] _RangeSO;
    private int _EnemiesAlive;
    [SerializeField]
    private int _InitEnemyToSpawn = 4;
    public delegate void ChangeWave(int wave);
    public event ChangeWave OnChangeWave; 
    [SerializeField]
    private SimpleGameEvent _ChangeWave;
    private void Awake()
    {
        GameManager.Instance.Wave = 1;
        _EnemiesAlive = _InitEnemyToSpawn;
    }
    private void Start()
    {
        StartCoroutine(SpawnerEnemies());
        OnChangeWave?.Invoke(GameManager.Instance.Wave);
    }
    private void IncrementSpawner()
    {
        GameManager.Instance.Wave += 1;
        _InitEnemyToSpawn += 1;
        _EnemiesAlive = _InitEnemyToSpawn;
        OnChangeWave?.Invoke(GameManager.Instance.Wave);
        _ChangeWave.Raise();
    }
    IEnumerator IncrementWave()
    {
        yield return new WaitForSeconds(4f);
        IncrementSpawner();
        StartCoroutine(SpawnerEnemies());
    }
    public void GameEventListenerEnemys()
    {
        _EnemiesAlive--;
        if (_EnemiesAlive == 0)
        {
            StartCoroutine(IncrementWave());
        }
    }
    IEnumerator SpawnerEnemies()
    {
        yield return new WaitForSeconds(1);
        if (GameManager.Instance.Wave == 1)
        {
            for (int i = 0; i < _InitEnemyToSpawn; i++)
            {
                SpawnMeleeEnemies();
            }
        }
        else if (GameManager.Instance.Wave == 2)
        {
            for (int i = 0; i < _InitEnemyToSpawn; i++)
            { 
                SpawnRangeEnemies();
            }
        }
        else if (GameManager.Instance.Wave >= 3)
        {
            for (int i = 0; i < _InitEnemyToSpawn; i++)
            {
                int RandomR = Random.Range(0,2);
                if (RandomR == 1) 
                {
                    SpawnMeleeEnemies();
                }
                else
                {
                    SpawnRangeEnemies();
                }
            }
        }
    }
    private void SpawnMeleeEnemies()
    {
        int t = Random.Range(0, _MeleeSO.Length);
        int b = Random.Range(0,_SpawnPoint.Length);
        GameObject go = Instantiate(_MeleeEnemy);
        go.GetComponent<EnemySM>().Init(_MeleeSO[t], _SpawnPoint[b]);
    }
    private void SpawnRangeEnemies()
    {
        int t = Random.Range(0, _RangeSO.Length);
        int b = Random.Range(0, _SpawnPoint.Length);
        GameObject go = Instantiate(_RangedEnemy);
        go.GetComponent<EnemyRangeSM>().Init(_WaypointsRangeEnemy, _SpawnPoint[b], _RangeSO[t]);
    }
}
