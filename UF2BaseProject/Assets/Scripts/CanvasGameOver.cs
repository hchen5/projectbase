using System.Collections;
using System.Collections.Generic;
using TMPro;
using UF2BaseProject;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasGameOver : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _Wave;

    private void Start()
    {
        _Wave.SetText("Wave: " + GameManager.Instance.Wave);
    }
    public void Replay()
    {
        SceneManager.LoadScene("Game");
    }
}
