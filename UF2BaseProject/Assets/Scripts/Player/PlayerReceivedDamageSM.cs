using System.Collections;
using System.Collections.Generic;
using UF2BaseProject;
using UnityEngine;
using static UF2BaseProject.Player;

public class PlayerReceivedDamageSM : MBState
{
    private Player _Player;
    private Rigidbody2D _Rigidbody;
    private Animator _Animator;
    private MBStateMachine _StateMachine;
    private void Awake()
    {
        _Player = GetComponent<Player>();
        _Animator = GetComponent<Animator>();
        _StateMachine = GetComponent<MBStateMachine>();
        _Rigidbody = GetComponent<Rigidbody2D>();
    }
    public override void Exit()
    {
    }

    public override void Init()
    {
        _Rigidbody.velocity = Vector2.zero;
        _Animator.Play("JinReceivedDamage");
    }
    public void Ended()
    {
        _StateMachine.ChangeState<PlayerIdleSM>();
    }

}
