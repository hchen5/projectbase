using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UF2BaseProject
{
    public class PlayerAttack2SM : SMBComboState
    {
        public override void Init()
        {
            base.Init();
            m_Animator.Play("JinAttack2");
        }
        protected override void OnComboFailedAction()
        {
        }
        protected override void OnComboSuccessAction()
        {
            GetComponent<PlayerAtack1SM>().ComboSuccessBonus();
            m_StateMachine.ChangeState<PlayerAtack1SM>();
        }

        protected override void OnEndAction()
        {
            m_StateMachine.ChangeState<PlayerIdleSM>();
        }
        public override void Exit()
        {
            base.Exit();
            GetComponent<PlayerAttack2SM>().NormalDamage();
        }
    }
}
