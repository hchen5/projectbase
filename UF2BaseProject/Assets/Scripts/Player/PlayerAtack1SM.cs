using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UF2BaseProject
{
    public class PlayerAtack1SM : SMBComboState
    {
        public override void Init()
        {
            base.Init();
            m_Animator.Play("JinAttack1");
        }
        protected override void OnComboFailedAction()
        {
        }
        protected override void OnComboSuccessAction()
        {
            GetComponent<PlayerAttack2SM>().ComboSuccessBonus();
            m_StateMachine.ChangeState<PlayerAttack2SM>();
        }
        protected override void OnEndAction()
        {
            m_StateMachine.ChangeState<PlayerIdleSM>();
        }
        public override void Exit()
        {
            base.Exit();
            GetComponent<PlayerAtack1SM>().NormalDamage();
        }
    }
}
