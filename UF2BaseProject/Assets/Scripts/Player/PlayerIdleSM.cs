using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace UF2BaseProject
{
    public class PlayerIdleSM : MBState
    {
        private Player _Player;
        private Rigidbody2D _Rigidbody;
        private Animator _Animator;
        private MBStateMachine _StateMachine;
        private void Awake()
        {
            _Player = GetComponent<Player>();
            _Animator = GetComponent<Animator>();
            _StateMachine = GetComponent<MBStateMachine>();
            _Rigidbody = GetComponent<Rigidbody2D>();
        }
        public override void Exit()
        {
            _Player.Input.FindActionMap("Player").FindAction("Attack1").performed -= Attack1;
            _Player.Input.FindActionMap("Player").FindAction("Attack2").performed -= Attack2;
        }
        public override void Init()
        {
            _Player.Input.FindActionMap("Player").FindAction("Attack1").performed += Attack1;
            _Player.Input.FindActionMap("Player").FindAction("Attack2").performed += Attack2;
            _Rigidbody.velocity = Vector2.zero;
            _Animator.Play("JinIdleAnim");
        }
        private void Attack1(InputAction.CallbackContext context) 
        {
            _StateMachine.ChangeState<PlayerAtack1SM>();
        }
        private void Attack2(InputAction.CallbackContext context) 
        {
            _StateMachine.ChangeState<PlayerAttack2SM>();
        }
        private void Update()
        {
            if (_Player.MovementAction.ReadValue<Vector2>() != Vector2.zero)
            {
                _StateMachine.ChangeState<PlayerWalkSM>();
            }
        }

    }
}
