using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace UF2BaseProject 
{
    public class PlayerWalkSM : MBState
    {
        private Player _Player;
        private Rigidbody2D _Rigidbody;
        private Animator _Animator;
        private MBStateMachine _StateMachine;
        [SerializeField]
        private float _Speed = 5f;
        private Vector2 _Movement;
        private void Awake()
        {
            _Player = GetComponent<Player>();
            _Animator = GetComponent<Animator>();
            _StateMachine = GetComponent<MBStateMachine>();
            _Rigidbody = GetComponent<Rigidbody2D>();
        }
        public override void Exit()
        {
            _Player.Input.FindActionMap("Player").FindAction("Attack1").performed -= Attack1;
            _Player.Input.FindActionMap("Player").FindAction("Attack2").performed -= Attack2;
        }
        public override void Init()
        {
            _Player.Input.FindActionMap("Player").FindAction("Attack1").performed += Attack1;
            _Player.Input.FindActionMap("Player").FindAction("Attack2").performed += Attack2;
            _Animator.Play("JinWalkAnim");
        }
        private void Update()
        {
            _Movement = _Player.MovementAction.ReadValue<Vector2>();
            if (_Movement.x == -1) 
            {
                transform.localEulerAngles = Vector3.up * 180;
            }else if (_Movement.x == 1) 
            {
                transform.localEulerAngles = Vector3.up * 0;
            }
            if (_Movement == Vector2.zero) 
            {
                _StateMachine.ChangeState<PlayerIdleSM>();
            }
        }
        private void Attack1(InputAction.CallbackContext context) 
        {
            _StateMachine.ChangeState<PlayerAtack1SM>();
        }
        private void Attack2(InputAction.CallbackContext context) 
        {
            _StateMachine.ChangeState<PlayerAttack2SM>();
        }
        private void FixedUpdate()
        {
            _Rigidbody.velocity = _Movement.normalized * _Speed;
        }
    }
}
