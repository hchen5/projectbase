using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.InputSystem;
using UnityEngine.Windows;
using static UnityEditor.Experimental.GraphView.GraphView;

namespace UF2BaseProject
{
    [RequireComponent(typeof(MBStateMachine))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(BoxCollider2D))]
    [RequireComponent (typeof(PlayerIdleSM))]
    [RequireComponent (typeof(PlayerWalkSM))]
    [RequireComponent (typeof(PlayerAtack1SM))]
    public class Player : MonoBehaviour
    {
        private MBStateMachine _StateMachine;
        [SerializeField]
        private InputActionAsset _PlayerActionAssets;
        private InputActionAsset _PlayerInputs;
        public InputActionAsset Input => _PlayerInputs;
        private InputAction _MovementAction;
        public InputAction MovementAction => _MovementAction;
        [SerializeField]
        private int _Health = 15;
        public delegate void HealthChange(int health);
        public event HealthChange OnHealthChange;
        private Transform _PlayerPos;
        public void ResetPlayerPos() 
        {
            transform.position = new Vector3(-7.95f, -3.08f, 0);
        }
        private void Awake()
        {
            _PlayerInputs = Instantiate(_PlayerActionAssets);
            _PlayerInputs.FindActionMap("Player").Enable();
            _MovementAction = _PlayerInputs.FindActionMap("Player").FindAction("WalkWASD");
            _StateMachine = GetComponent<MBStateMachine>();
            
        }
        private void Start()
        {
            _PlayerPos = transform;
            _StateMachine.ChangeState<PlayerIdleSM>(); OnHealthChange?.Invoke(_Health);
        }
        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.layer == LayerMask.NameToLayer("EnemyHitBox"))
            {
                if (collision.gameObject.GetComponentInParent<EnemySM>() != null)
                {
                    SubstractHealth(collision.gameObject.GetComponentInParent<EnemySM>().AttackDMG);
                    int r = Random.Range(0, 2);
                    if(r == 1)
                        _StateMachine.ChangeState<PlayerReceivedDamageSM>();
                }
                if (collision.gameObject.GetComponentInParent<EnemyRangeHitbox>() != null)
                {
                    SubstractHealth(collision.gameObject.GetComponentInParent<EnemyRangeHitbox>().Damage);
                    int r = Random.Range(0, 2);
                    if (r == 1)
                        _StateMachine.ChangeState<PlayerReceivedDamageSM>();
                }
            }
        }
        public void SubstractHealth(int health)
        {
            if (_Health - health > 0)
                _Health -= health;
            else
                _Health = 0;

            if (_Health == 0)
            {
                Destroy(gameObject);
                GameManager.Instance.GameLoss();
            }
            OnHealthChange?.Invoke(_Health);
        }
    }
}
