using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;

namespace UF2BaseProject
{
    [RequireComponent(typeof(ComboHandler))]
    public abstract class SMBComboState : MBState
    {
        private Player m_PJ;
        private Rigidbody2D m_Rigidbody;
        protected Animator m_Animator;
        protected MBStateMachine m_StateMachine;
        private ComboHandler m_ComboHandler;


        [SerializeField]
        private PlayerHitBox m_Hitbox;
        [SerializeField]
        private int m_InitDamage;
        protected int m_Damage;

        private void Awake()
        {
            Assert.IsNotNull(m_Hitbox);
            m_PJ = GetComponent<Player>();
            m_Rigidbody = GetComponent<Rigidbody2D>();
            m_Animator = GetComponent<Animator>();
            m_StateMachine = GetComponent<MBStateMachine>();
            m_ComboHandler = GetComponent<ComboHandler>();
            m_Damage = m_InitDamage;
        }

        public override void Init()
        {
            m_ComboHandler.enabled = true;
            m_PJ.Input.FindActionMap("Player").FindAction("Attack1").performed += OnAttack;
            m_PJ.Input.FindActionMap("Player").FindAction("Attack2").performed += OnAttack2;
            m_Rigidbody.velocity = Vector2.zero;
            m_Hitbox.DMG = m_Damage;
            m_ComboHandler.OnEndAction += OnEndAction;
        }

        public override void Exit()
        {
            m_ComboHandler.enabled = false;
            m_PJ.Input.FindActionMap("Player").FindAction("Attack1").performed -= OnAttack;
            m_PJ.Input.FindActionMap("Player").FindAction("Attack2").performed -= OnAttack2;
            m_ComboHandler.OnEndAction -= OnEndAction;
        }

        private void OnAttack(InputAction.CallbackContext context)
        {
            if (m_ComboHandler.ComboAvailable)
                OnComboSuccessAction();
            else
                OnComboFailedAction();
        }
        private void OnAttack2(InputAction.CallbackContext context)
        {
            if (m_ComboHandler.ComboAvailable)
                OnComboSuccessAction();
            else
                OnComboFailedAction();
        }
        public void ComboSuccessBonus() 
        {
            m_Damage *= 2;
        }
        public void NormalDamage() 
        {
            m_Damage = m_InitDamage;
        }
        protected abstract void OnComboSuccessAction();
        protected abstract void OnComboFailedAction();

        protected abstract void OnEndAction();
    }
}
